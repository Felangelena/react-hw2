import { useState, useEffect } from 'react';
import './App.scss';
import {ReactComponent as Favorites} from './favorites.svg';
import {ReactComponent as Cart} from './cart.svg';
import ProductList from './components/productList/productList.jsx';
import Button from './components/btn/button.jsx';
import Modal from './components/modal/modal.jsx';

function App() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [data, setData] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [cart, setCart] = useState([]);
  const [favorite, setFavorite] = useState([]);
  const [id, setId] = useState(0);

  useEffect(() => {
    fetch('./storeData.json')
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setData(result);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, []);

  const addToCart = (id) => {
    setCart((prev) => {
      localStorage.setItem('cart', JSON.stringify([...prev, id]));
      return [...prev, id];
    })
    closeModalWin();
  }

  const addToFavorite = ({e, id}) => {
    e.target.classList.add('active');
    setFavorite((prev) => {
      localStorage.setItem('favorite', JSON.stringify([...prev, id]));
      return [...prev, id];
    })
  }

  const openModalWin = ({id}) => {
    setId(id);
    document.body.classList.add('noscroll');
    setOpenModal(true);
  }

  const closeModalWin = () => {
    document.body.classList.remove('noscroll');
    setOpenModal(false);
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {

  return (
    <>
      <header className='header box'>
      <Favorites/><span className='favorites'>{favorite.length}</span>
      <Cart/><span className='cart'>{cart.length}</span>
      </header>
      <ProductList data={data} addToFavorite={addToFavorite}
      openModalWin={openModalWin}/>
      {openModal && <Modal header="Add item" isCloseButton={true} text="Do you want add item to the cart?" close={closeModalWin}
      actions={[
          <Button key="1" className={'modal__btn'} backgroundColor="maroon" text="Ok" handleClick={() => {addToCart(id)}}/>,
          <Button key="2" className={'modal__btn'} backgroundColor="maroon" text="Cancel" handleClick={closeModalWin}/>
      ]} />
      }
    </>
  );
  }
}

export default App;
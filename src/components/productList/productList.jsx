import PropTypes from "prop-types";
import './productList.scss';
import ProductListItem from '../productListItem/productListItem.jsx';

const ProductList = ({data, openModalWin, addToFavorite}) => {

    return (
        <section className="arrivals box">
            <h2 className="arrivals__title">Latest arrivals</h2>
            <ul className="arrivals__grid">
                {data.map(item => (
                    <ProductListItem key={item.id} id={item.id}  title={item.title} color={item.color} price={item.price} image={item.image}
                    openModalWin={openModalWin} addToFavorite={addToFavorite}
                    />
                ))}
            </ul>

        </section>
    )
}

ProductList.propTypes = {
    data: PropTypes.array.isRequired,
    openModalWin: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired
}

export default ProductList;
import PropTypes from "prop-types";
import './productListItem.scss';
import {ReactComponent as Star} from '../../star.svg';
import Button from '../btn/button.jsx';

const ProductListItem = ({id, title, color, price, image, openModalWin, addToFavorite}) => {

    return (
        <li className="card">
            <div className="card__favorite"><Star className="star" onClick={(e) => {addToFavorite({id, e})}}/></div>
            <img className="card__img" src={image} alt={title} />
            <h3 className="card__title">{title}</h3>
            <p className="card__color">Color: {color}</p>
            <p className="card__price">${price}</p>
            <Button className="card__btn" backgroundColor="black" text="Add to cart" handleClick={() => {openModalWin({id})}}/>
        </li>
    )
}

ProductListItem.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    openModalWin: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired
}

ProductListItem.defaultProps = {
    color: "black",
    image: "image don't exist",
}

export default ProductListItem;